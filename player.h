#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <stdio.h>
#include <vector>

using namespace std;

struct Node {
    int value;
    bool vset;
    Node *parent;
    vector<Node*> children;
    Board *board;
    int x, y;
    Side turn;
    Side player;
    
    Node(Node *parent, Board *board, Side turn, Side player, int x = -1, int y = -1) {
        this->parent = parent;
        this->board = board;
        this->x = x;
        this->y = y;
        this->turn = turn;
        this->player = player;
		vset = false;
    }
    
    ~Node() {
		for(unsigned int i = 0; i < children.size(); i++) {
			delete children[i];
		}
		delete board;
	}
    
    void insert(Node *child) {
		children.push_back(child);
    }
    
    bool hasChild() {
		return children.size() == 0;
	}
	
	int calcValue() {
		if(children.size() == 0) {
			cerr<<"Calculating frontier score: "<<board->getScore(player)<<endl;
			value = board->getScore(player);
			return value;
		}
		for(unsigned int i = 0; i < children.size(); i++) {
			if(!vset) {
				value = children[i]->calcValue();
				vset = true;
			} else {
				if(turn == player) {
					if(children[i]->calcValue() < value) {
						value = children[i]->value;
					}
				} else {
					if(children[i]->calcValue() > value) {
						value = children[i]->value;
					}
				}
			}
		}
		
		return value;
	}
	
};

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Side mySide;
    Side opSide;
    
    Board board;
    
    Move *bestMove;
};

#endif
