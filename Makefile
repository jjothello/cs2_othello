CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb
OBJS        = player.o board.o
PLAYERNAME  = jjothello
PLAYERNAME2  = player2

all: $(PLAYERNAME) $(PLAYERNAME) testgame
	
$(PLAYERNAME): $(OBJS) wrapper.o
	$(CC) -o $@ $^

$(PLAYERNAME2): $(OBJS) wrapper.o
	$(CC) -o $@ $^

testgame: testgame.o
	$(CC) -o $@ $^

testminimax: $(OBJS) testminimax.o
	$(CC) -o $@ $^

%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@
	
java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(PLAYERNAME) $(PLAYERNAME2) testgame testminimax
	
.PHONY: java testminimax
