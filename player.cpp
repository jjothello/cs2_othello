#include "player.h"

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    mySide = side;
    opSide = mySide == WHITE ? BLACK : WHITE;
	board = Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	if(bestMove != NULL) {delete bestMove;}
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    board.doMove(opponentsMove, opSide);
    
    vector<Node*> tree;
    int frontier = 0, originalFrontier = 0;
    
    // find possible moves
    for(int i = 0; i < 64; i++) {
		Move m(i/8, i%8);
		if(board.checkMove(&m, mySide)) {
			cerr << "(" << m.x << ", " << m.y << ") is a possible first move." << endl;
			Board *nBoard = board.copy();
			nBoard->doMove(&m, mySide);
			tree.push_back(new Node(NULL, nBoard, mySide, mySide, m.x, m.y));
			originalFrontier++;
		}
	}
    
    // build tree
    int ply = 6, moves = 1;
    if(testingMinimax) {ply = 2;}
    Side turn = opSide;
    while(moves < ply) {
		int nextFrontier = frontier;
		int end = tree.size();
		
		for(int i = frontier; i < end; i++) {
			bool canMove = false;
			for(int j = 0; j < 64; j++) {
				Move m = Move(j/8, j%8);
				if(tree[i]->board->checkMove(&m, turn)) {
//					cerr<<"adding "<<m.x<<","<<m.y<<" by "<<(turn == BLACK ? "BLACK" : "WHITE")<<" to tree after "<<tree[i]->x<<", "<<tree[i]->y<<endl;
					Board *nBoard = tree[i]->board->copy();
					nBoard->doMove(&m, turn);
					Node *n = new Node(tree[i], nBoard, turn, mySide, m.x, m.y);
					tree[i]->children.push_back(n);
					tree.push_back(n);
					canMove = true;
				}
			}
			if(!canMove) {
//				cerr<<(turn == BLACK ? "BLACK" : "WHITE")<<" can pass after "<<tree[i]->x<<", "<<tree[i]->y<<endl;
				Board *nBoard = tree[i]->board->copy();
				Node *n = new Node(tree[i], nBoard, turn, mySide);
				tree[i]->children.push_back(n);
				tree.push_back(n);
			}
			nextFrontier++;
		}
		
		if(frontier == nextFrontier) {break;}
		frontier = nextFrontier;
		turn = turn == mySide ? opSide : mySide;
		moves++;
	}
	
/*    // Set values of frontier
    cerr<<"BLAH "<<tree.size()-frontier<<endl;
	for(unsigned int i = frontier; i < tree.size(); i++) {
		tree[i]->value = tree[i]->board->getScore(mySide);
		if(testingMinimax) {tree[i]->value = tree[i]->board->count(mySide)-tree[i]->board->count(opSide);}
		tree[i]->vset = true;
	}*/
    
    // Calc values of original frontier and find the maximum
    int maxValue = -1000000;
    //int nmbrMax = 0;
    Node *moveNode = NULL;
    for(int i = 0; i < originalFrontier; i++) {
		tree[i]->calcValue();
		if(tree[i]->value > maxValue) {
			maxValue = tree[i]->value;
			moveNode = tree[i];
		//	nmbrMax = 1;
		}
		//else if(tree[i]->value = maxValue)
		//{
			//nmbrMax ++;
		//}
//		cerr<<"Assining ("<<tree[i]->x<<","<<tree[i]->y<<") a value of "<<tree[i]->value<<endl;
	}
	
	if(moveNode == NULL) {
		return NULL;
	}
	
	cerr<<"Move determined, score is "<<moveNode->value<<endl;
	bestMove = new Move(moveNode->x, moveNode->y);
	board.doMove(bestMove, mySide);
	for(int i = originalFrontier - 1; i >=0; i--) {
		delete tree[i];
	}
    return bestMove;
}
